const favoriteMovies = JSON.parse(localStorage.getItem('favorite')) || { movies: [] }

class Film {
    constructor(film, id){
        this.id = id
        this.film = film
        this.favorite = this.getIsFavorite()
        this.domElement = this.render()
    }

    render(){
        const {
            Poster,
            Title,
            Year,
            Plot,
            Genre,
            Actors,
            Runtime,
            imdbRating
        } = this.film
        return `
            <div class="film-card">
                <div class="film-card__header">
                    <h3>${Title}</h3>
                    <span>${Year}</span>
                    <button class="back">Back</button>
                    <button class="favorite-btn">${this.favorite ? 'Delete from favorite' : 'Add from favorite'}</button>
                </div>
                <div class="film-card__info">
                    <img src="${Poster !== 'N/A' ? Poster : './noimg.jpg'}" alt="No poster">
                    <span>Genre: ${Genre}</span>
                    <span>Actors: ${Actors}</span>
                    <span>Plot: ${Plot}</span>
                    <span>Runtime: ${Runtime}</span>
                    <span>ImdbRating: ${imdbRating}</span>
                </div>
            </div>
        `
    }

    async getFilm(){
        const film = await Api.getFilmsByQuery(`?i=${this.id}`)
        return film
    }

    getIsFavorite(){
        const currentMovie = favoriteMovies.movies.find(film => film.imdbID === this.id)
        return Boolean(currentMovie)
    }

    renderIn(domElement){
        document.querySelector('.search-page').style.display = 'none'
        domElement.innerHTML = this.domElement
        this.addExit()
        this.addFavorite()
    }

    addExit(){
        document.querySelector('.back').onclick = () => {
            document.querySelector('.film-card').style.display = 'none'
            document.querySelector('.search-page').style.display = 'block'
            const list = new FilmList(favoriteMovies.movies)
            list.renderIn(document.querySelector('.favorite'))
        }
    }

    addFavorite(){
        document.querySelector('.favorite-btn').onclick = (e) => {
            this.favorite = !this.favorite
            console.log('here')
            if (this.favorite){
                favoriteMovies.movies.push(this.film)
                e.target.innerHTML = 'delete from favorite'
            } else {
                const currentFilmIndex = favoriteMovies.movies.findIndex(film => {
                    return film.imdbID === this.id
                })
                console.log(currentFilmIndex)
                favoriteMovies.movies.splice(currentFilmIndex, 1)
                e.target.innerHTML = 'add to favorite'
            }
            localStorage.setItem('favorite', JSON.stringify(favoriteMovies))
        }
    }
}